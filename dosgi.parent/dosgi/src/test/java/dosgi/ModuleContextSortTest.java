/**
 * 
 */
package dosgi;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import com.dosgi.module.IModuleContext;
import com.dosgi.module.ModuleContext;

/**
 * @author dingnate
 *
 */
public class ModuleContextSortTest {

	private static HashMap<String, IModuleContext> map;

	private static void sortModules(LinkedList<IModuleContext> mContexts) {
		if(mContexts.isEmpty())return;
		map = new HashMap<String, IModuleContext>();
		for (IModuleContext mContext : mContexts)
			map.put(mContext.getSymbolicName(), mContext);
		HashSet<IModuleContext> passed = new HashSet<IModuleContext>();
		for (int i = 0; i < mContexts.size(); i++) {
			pushRequiredForward(mContexts.get(i), mContexts, passed);
		}
	}

	private static void pushRequiredForward(IModuleContext mContext,
			LinkedList<IModuleContext> mContexts,
			Collection<IModuleContext> passed) {
		if (passed.contains(mContext))
			return;
		passed.add(mContext);
		Collection<IModuleContext> requireModules = getAllRequiredModules(mContext);
		if (requireModules.isEmpty()) {
			if (mContexts.getFirst() == mContext)
				return;
			mContexts.remove(mContext);
			mContexts.addFirst(mContext);
			return;
		}
		int index = mContexts.indexOf(mContext);
		for (IModuleContext rMContext : requireModules) {
			int indexR = mContexts.indexOf(rMContext);
			if (indexR > index) {
				mContexts.remove(indexR);
				mContexts.add(index, rMContext);
			}
			pushRequiredForward(rMContext, mContexts, passed);
		}
	}

	private static Collection<IModuleContext> getAllRequiredModules(IModuleContext mContext) {
		switch (mContext.getSymbolicName()) {
		case "a":
			return new HashSet();
		case "b":
			return new HashSet() {
				{
					add(map.get("a"));
				}
			};
		case "c":
			return new HashSet() {
				{
					add(map.get("b"));
				}
			};
		case "d":
			return new HashSet() {
				{
					add(map.get("c"));
				}
			};
		}
		return null;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LinkedList<IModuleContext> list = new LinkedList<IModuleContext>();
		list.add(new ModuleContext(new HashMap<String, String>() {
			{
				put("Implementation-Title", "d");
			}
		}, ""));
		list.add(new ModuleContext(new HashMap<String, String>() {
			{
				put("Implementation-Title", "c");
			}
		}, ""));
		list.add(new ModuleContext(new HashMap<String, String>() {
			{
				put("Implementation-Title", "b");
			}
		}, ""));
		list.add(new ModuleContext(new HashMap<String, String>() {
			{
				put("Implementation-Title", "a");
			}
		}, ""));

		sortModules(list);
		for (IModuleContext mContext : list) {
			System.out.println(mContext.getSymbolicName());
		}
	}

}
