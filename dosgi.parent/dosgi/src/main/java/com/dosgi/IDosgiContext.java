package com.dosgi;

import java.util.Set;

import com.dosgi.bean.IBeanFactory;
import com.dosgi.clazz.ClassHandler;
import com.dosgi.module.IModuleContext;

/**
 * 应用上下文
 * @author dingnate
 *
 */
public interface IDosgiContext {

	/**
	 * 启动上下文
	 * @return
	 * @throws Throwable
	 */
	public IDosgiContext start() throws Throwable;

	/**
	 * 上下文销毁
	 * @throws Throwable
	 */
	public void destroy() throws Throwable;
	
	/**
	 * Bean工厂
	 * @return
	 */
	public IBeanFactory beanFactory();

	/**
	 * 注册module
	 * 
	 * @param mooduleContext
	 */
	public void registerModule(IModuleContext mooduleContext);

	/**
	 * 获取module
	 * 
	 * @param symbolicName
	 * @param version
	 * @return
	 */
	public IModuleContext getModule(String symbolicName, String version);
	
	/**
	 * 通过ExportPackage获取module
	 * 
	 * @param packageName
	 * @param version
	 * @return
	 */
	public IModuleContext getModuleByExportPackage(String packageName, String version);

	/**
	 * 添加类处理器
	 * @param classHandler
	 */
	public void addClassHandler(ClassHandler classHandler);
	
	/**
	 * 获取所有的已添加的类处理器
	 * 
	 * @return
	 */
	public Set<ClassHandler> getClassHandlers();
}
