package com.dosgi.conf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.conf4j.Conf;
import com.conf4j.annotation.Config;
import com.conf4j.kit.ConfKit;

/**
 * dosgi线程池配置
 * 
 * @author dingnate
 *
 */
@Config(file = "dosgi.properties", prefix = "dosgi.pool")
public class DosgiPoolConf implements Conf {
	private static transient final Logger LOG = LoggerFactory.getLogger(DosgiPoolConf.class);
	public final static DosgiPoolConf me = new DosgiPoolConf();
	static {
		try {
			ConfKit.handler(DosgiPoolConf.class);
		} catch (Exception e) {
			LOG.error("handler DosgiPoolConf failed.", e);
		}
	}
	private int workQueueCapacity;
	private int maximumPoolSize;
	private long keepAliveTime;
	private String timeUnit;
	private String prefix;

	public int getWorkQueueCapacity() {
		return workQueueCapacity;
	}

	public void setWorkQueueCapacity(int workQueueCapacity) {
		this.workQueueCapacity = workQueueCapacity;
	}

	public final int getMaximumPoolSize() {
		return maximumPoolSize;
	}

	public final void setMaximumPoolSize(int maximumPoolSize) {
		this.maximumPoolSize = maximumPoolSize;
	}

	public final long getKeepAliveTime() {
		return keepAliveTime;
	}

	public final void setKeepAliveTime(long keepAliveTime) {
		this.keepAliveTime = keepAliveTime;
	}

	public final String getTimeUnit() {
		return timeUnit;
	}

	public final void setTimeUnit(String timeUnit) {
		this.timeUnit = timeUnit;
	}

	public final String getPrefix() {
		return prefix;
	}

	public final void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@Override
	public String toString() {
		return "DosgiPoolConf [workQueueCapacity=" + workQueueCapacity + ", maximumPoolSize=" + maximumPoolSize
				+ ", keepAliveTime=" + keepAliveTime + ", timeUnit=" + timeUnit + ", prefix=" + prefix + "]";
	}
}
